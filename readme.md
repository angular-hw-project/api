# Task Control API

This API provides CRUD functionality for:

- Task Boards;
- Tasks;
- Task Comments.

User authentication is mandatory for access to the API features.