const express = require('express');

const { commentExistMiddleware } = require('../middleware/commentExistMiddleware');

const {
  getComments,
  createComment,
  deleteComment,
} = require('../controllers/commentsController');

const router = express.Router();

router.get('/:task_id', getComments);
router.post('/', createComment);
router.delete('/:id', commentExistMiddleware, deleteComment);

module.exports = {
  commentsRouter: router,
};
