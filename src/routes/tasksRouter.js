const express = require('express');

const { taskExistMiddleware } = require('../middleware/taskExistMiddleware');
const { boardExistMiddleware } = require('../middleware/boardExistMiddleware');

const {
  getTasks,
  // getTask,
  createTask,
  toggleArchivedStatus,
  deleteTask,
  editTask,
} = require('../controllers/tasksController');

const router = express.Router();

router.get('/board/:id', boardExistMiddleware, getTasks);
// router.get('/:id', taskExistMiddleware, getTask);
router.post('/', createTask);
router.put('/:id', taskExistMiddleware, editTask);
router.patch('/:id/archived/toggle', taskExistMiddleware, toggleArchivedStatus);
router.delete('/:id', taskExistMiddleware, deleteTask);

module.exports = {
  tasksRouter: router,
};
