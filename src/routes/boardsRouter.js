const express = require('express');

const { boardExistMiddleware } = require('../middleware/boardExistMiddleware');

const {
  getBoard,
  getBoards,
  createBoard,
  editBoard,
  patchBoardColor,
  deleteBoard,
} = require('../controllers/boardsController');

const router = express.Router();

router.get('/', getBoards);
router.get('/:id', boardExistMiddleware, getBoard);
router.post('/', createBoard);
router.put('/:id', boardExistMiddleware, editBoard);
router.patch('/:id/colors/:color', boardExistMiddleware, patchBoardColor);
router.delete('/:id', boardExistMiddleware, deleteBoard);

module.exports = {
  boardsRouter: router,
};
