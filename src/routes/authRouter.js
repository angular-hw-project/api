const express = require('express');

const router = express.Router();

const { authMiddleware } = require('../middleware/authMiddleware');

const {
  registerUser,
  loginUser,
  getUser,
} = require('../controllers/authController');

router.post('/register', registerUser);
router.post('/login', loginUser);
router.get('/', authMiddleware, getUser);

module.exports = {
  authRouter: router,
};
