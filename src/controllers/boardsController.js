const { Board } = require('../models/Board');
const { Task } = require('../models/Task');

const boardOutput = (board) => ({
  // eslint-disable-next-line no-underscore-dangle
  _id: board._id,
  created_by: board.created_by,
  name: board.name,
  description: board.description,
  color_scheme: board.color_scheme,
  tasks: {
    new: board.tasks.filter((task) => task.status === 'new' && !task.archived),
    inProgress: board.tasks.filter((task) => task.status === 'inProgress' && !task.archived),
    done: board.tasks.filter((task) => task.status === 'done' && !task.archived),
    archived: board.tasks.filter((task) => task.archived),
  },
  createdDate: board.createdDate,
});

const getBoards = async (req, res) => {
  try {
    let filters = {};
    if (req.query.filterType === 'name') {
      filters = { name: new RegExp(req.query.filterText, 'i') };
    }
    if (req.query.filterType === 'taskName') {
      const tasks = await Task.find({ name: new RegExp(req.query.filterText, 'i') });
      // eslint-disable-next-line no-underscore-dangle
      filters = { _id: { $in: tasks.map((task) => task.board_id) } };
    }
    const boards = await Board.find(filters).populate('tasks');
    res.status(200).send(boards.map((board) => boardOutput(board)));
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

const getBoard = async (req, res) => {
  try {
    const board = await Board.findById(req.params.id).populate('tasks');
    res.status(200).send(boardOutput(board));
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

const createBoard = async (req, res) => {
  try {
    await new Board({
      created_by: req.user.id,
      name: req.body.name,
      description: req.body.description,
    }).save();

    res.status(200).send({ message: 'Board created successfully' });
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

const editBoard = async (req, res) => {
  try {
    const newData = {
      name: req.body.name,
      description: req.body.description,
    };

    await Board.findByIdAndUpdate(req.params.id, newData);
    res.status(200).send({ message: 'Board details changed successfully' });
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

const patchBoardColor = async (req, res) => {
  try {
    const newData = {
      color_scheme: {
        [req.params.color]: req.body.color,
      },
    };

    await Board.findByIdAndUpdate(req.params.id, newData);
    res.status(200).send({ message: `Board "${req.params.color}" color updated successfully` });
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

const deleteBoard = async (req, res) => {
  try {
    await Board.findByIdAndDelete(req.params.id);
    res.status(200).send({ message: 'Board deleted successfully' });
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

module.exports = {
  getBoards,
  getBoard,
  createBoard,
  editBoard,
  patchBoardColor,
  deleteBoard,
};
