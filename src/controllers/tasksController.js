const { Task } = require('../models/Task');

const getTasks = async (req, res) => {
  try {
    let filters = { };
    if (req.query.filterType === 'name') {
      filters = {
        board_id: req.params.id,
        name: new RegExp(req.query.filterText, 'i'),
      };
    }
    const tasks = await Task.find(filters).populate('comments');
    res.status(200).send([...tasks]);
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

const getTask = async (req, res) => {
  try {
    const task = await Task.findById(req.params.id).populate('comments');
    res.status(200).send(task[0]);
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

const createTask = async (req, res) => {
  try {
    await new Task({
      created_by: req.user.id,
      board_id: req.body.board_id,
      name: req.body.name,
      status: req.body.status,
    }).save();

    res.status(200).send({ message: 'Task created successfully' });
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

// TODO: Create getTasks method for extraction of all tasksn on board with comments

const deleteTask = async (req, res) => {
  try {
    await Task.findByIdAndDelete(req.params.id);
    res.status(200).send({ message: 'Task deleted successfully' });
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

const toggleArchivedStatus = async (req, res) => {
  try {
    const task = await Task.findById(req.params.id);
    task.archived = !task.archived;
    await task.save();
    const output = `Task "archived" status changed to "${task.archived.toString()}"`;
    res.status(200).send({ message: output });
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

const editTask = async (req, res) => {
  // Joi validation...

  try {
    const newData = {
      name: req.body.name,
      status: req.body.status,
    };

    await Task.findByIdAndUpdate(req.params.id, newData);
    res.status(200).send({ message: 'Task details changed successfully' });
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

module.exports = {
  getTasks,
  getTask,
  createTask,
  deleteTask,
  toggleArchivedStatus,
  editTask,
};
