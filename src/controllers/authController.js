const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { User } = require('../models/User');

const registerUser = async (req, res) => {
  const { email, username, password } = req.body;

  try {
    const duplicate = await User.findOne({ email });
    if (duplicate !== null) {
      res.status(400).send({ message: 'This user already exists' });
      return;
    }

    await new User({
      email,
      username,
      password: await bcrypt.hash(password, 10),
    }).save();

    res.status(201).send({ message: 'Success' });
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

const loginUser = async (req, res) => {
  try {
    const user = await User.findOne({ email: req.body.email });
    if (user === null) {
      res.status(401).send({ message: 'Not authorized' });
      return;
    }
    if (await bcrypt.compare(req.body.password, user.password) === false) {
      res.status(401).send({ message: 'Not authorized' });
      return;
    }

    const payload = {
      /* eslint-disable-next-line no-underscore-dangle */
      userId: user._id,
      email: user.email,
      username: user.username,
      password: user.password,
    };
    const jwtToken = jwt.sign(payload, process.env.JWT_PASSWORD);
    res.status(200).send({
      message: 'Success',
      jwt_token: jwtToken,
    });
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

const getUser = async (req, res) => {
  try {
    const user = await User.findById(req.user.id);
    res.status(200).send(user);
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

module.exports = {
  registerUser,
  loginUser,
  getUser,
};
