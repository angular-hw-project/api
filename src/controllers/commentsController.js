const { Comment } = require('../models/Comment');

const getComments = async (req, res) => {
  try {
    const comments = await Comment.find({ task_id: req.params.task_id }).populate(['created_by', 'task_id']);
    res.status(200).send([...comments]);
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

const createComment = async (req, res) => {
  try {
    await new Comment({
      created_by: req.user.id,
      task_id: req.body.task_id,
      text: req.body.text,
    }).save();

    res.status(200).send({ message: 'Comment created successfully' });
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

const deleteComment = async (req, res) => {
  try {
    await Comment.findByIdAndDelete(req.params.id);
    res.status(200).send({ message: 'Comment deleted successfully' });
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

module.exports = {
  getComments,
  createComment,
  deleteComment,
};
