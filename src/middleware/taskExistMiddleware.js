const { Task } = require('../models/Task');

const taskExistMiddleware = async (req, res, next) => {
  try {
    const result = await Task.exists({ _id: req.params.id });
    if (result === null) {
      res.status(400).send({ message: 'Task doesn\'t exist' });
      return;
    }
    next();
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

module.exports = {
  taskExistMiddleware,
};
