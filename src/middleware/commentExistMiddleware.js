const { Comment } = require('../models/Comment');

const commentExistMiddleware = async (req, res, next) => {
  try {
    const result = await Comment.exists({ _id: req.params.id });
    if (result === null) {
      res.status(400).send({ message: 'Comment doesn\'t exist' });
      return;
    }
    next();
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

module.exports = {
  commentExistMiddleware,
};
