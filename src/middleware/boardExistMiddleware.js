const { Board } = require('../models/Board');

const boardExistMiddleware = async (req, res, next) => {
  try {
    const result = await Board.exists({ _id: req.params.id });
    if (result === null) {
      res.status(400).send({ message: 'Board doesn\'t exist' });
      return;
    }
    next();
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

module.exports = {
  boardExistMiddleware,
};
