const express = require('express');
const dotenv = require('dotenv');
const mongoose = require('mongoose');
const cors = require('cors');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const fs = require('fs');

const { authRouter } = require('./routes/authRouter');
const { boardsRouter } = require('./routes/boardsRouter');
const { tasksRouter } = require('./routes/tasksRouter');
const { commentsRouter } = require('./routes/commentsRouter');

const { authMiddleware } = require('./middleware/authMiddleware');

dotenv.config();
mongoose.connect(process.env.DB_CONNECTION_STRING);

const PORT = process.env.PORT || 8080;
const app = express();

app.use(cors());
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(morgan('tiny', { stream: fs.createWriteStream('./requests.log', { flags: 'a' }) }));

app.use('/api/auth', authRouter);
app.use('/api/boards', authMiddleware, boardsRouter);
app.use('/api/tasks', authMiddleware, tasksRouter);
app.use('/api/comments', authMiddleware, commentsRouter);

app.get('/health', (req, res) => {
  res.status(200).send({ message: '🦊API is working.' });
});

app.listen(PORT);
