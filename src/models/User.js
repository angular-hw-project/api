const { Schema, model } = require('mongoose');

const userSchema = new Schema({
  email: {
    type: String,
    unique: true,
    required: true,
  },
  username: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  createdDate: {
    type: String,
    default: () => new Date().toISOString(),
    required: true,
  },
});

const User = model('User', userSchema);

module.exports = {
  User,
};
