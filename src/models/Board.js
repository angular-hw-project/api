const { Schema, model } = require('mongoose');

const boardSchema = new Schema({
  created_by: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
  },
  color_scheme: {
    new: {
      type: String,
      default: '#e03c32',
      required: true,
    },
    inProgress: {
      type: String,
      default: '#ffd301',
      required: true,
    },
    done: {
      type: String,
      default: '#52b63e',
      required: true,
    },
  },
  createdDate: {
    type: String,
    default: () => new Date().toISOString(),
    required: true,
  },
});

boardSchema.virtual('tasks', {
  ref: 'Task',
  localField: '_id',
  foreignField: 'board_id',
});

const Board = model('Board', boardSchema);

module.exports = {
  Board,
};
