const { Schema, model } = require('mongoose');

const commentSchema = new Schema({
  created_by: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
  task_id: {
    type: Schema.Types.ObjectId,
    ref: 'Task',
  },
  text: {
    type: String,
    required: true,
  },
  createdDate: {
    type: String,
    default: () => new Date().toISOString(),
    required: true,
  },
});

commentSchema.virtual('user', {
  ref: 'User',
  localField: 'created_by',
  foreignField: '_id',
});

const Comment = model('Comment', commentSchema);

module.exports = {
  Comment,
};
