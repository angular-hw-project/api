const { Schema, model } = require('mongoose');

const taskSchema = new Schema({
  created_by: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
  board_id: {
    type: Schema.Types.ObjectId,
    ref: 'Board',
  },
  name: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    enum: [
      'new',
      'inProgress',
      'done',
    ],
    required: true,
  },
  archived: {
    type: Boolean,
    default: false,
    required: true,
  },
  createdDate: {
    type: String,
    default: () => new Date().toISOString(),
    required: true,
  },
}, {
  toJSON: { virtuals: true },
  toObject: { virtuals: true },
});

taskSchema.virtual('comments', {
  ref: 'Comment',
  localField: '_id',
  foreignField: 'task_id',
});

const Task = model('Task', taskSchema);

module.exports = {
  Task,
};
